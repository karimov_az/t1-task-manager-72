package ru.t1.karimov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.exception.entity.TaskNotFoundException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.model.Task;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Transactional
    public void add(@Nullable final Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Transactional
    public void addByUserId(@Nullable final String userId, @Nullable final Task model) {
        if (model == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUserId(userId);
        taskRepository.save(model);
    }

    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @NotNull
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    public List<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findAllByUserId(userId);
    }

    @Nullable
    public Task findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    public Task findOneByUserIdAndId(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findByUserIdAndId(userId, id);
    }

    @Transactional
    public void remove(@Nullable final Task model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.delete(model);
    }

    @Transactional
    public void removeAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteAllByUserId(userId);
    }

    @Transactional
    public void removeByUserId(@Nullable final String userId, @Nullable final Task model) {
        if (model == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteByUserIdAndId(userId, model.getId());
    }

    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteById(id);
    }

    @Transactional
    public void removeByUserIdAndId(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Transactional
    public void update(@Nullable final Task model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.save(model);
    }

}
