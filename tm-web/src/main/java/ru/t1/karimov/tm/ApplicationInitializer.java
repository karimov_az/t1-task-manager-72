package ru.t1.karimov.tm;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.karimov.tm.config.ApplicationConfiguration;
import ru.t1.karimov.tm.config.DatabaseConfiguration;
import ru.t1.karimov.tm.config.WebApplicationConfiguration;

/**
 * http://localhost:8080/ws/ProjectEndpoint.wsdl
 * http://localhost:8080/ws/TaskEndpoint.wsdl
 */

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { ApplicationConfiguration.class, DatabaseConfiguration.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { WebApplicationConfiguration.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

}
