package ru.t1.karimov.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.endpoint.ITaskEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public interface ITaskSoapEndpointClient {

    static ITaskEndpoint getInstance(@NotNull final String baseURL) throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/TaskEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "TaskRestEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.karimov.t1.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final ITaskEndpoint result = Service.create(url, name).getPort(ITaskEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
