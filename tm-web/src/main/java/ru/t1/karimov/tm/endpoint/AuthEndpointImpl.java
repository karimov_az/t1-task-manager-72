package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.model.Result;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.service.UserService;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.t1.karimov.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpointImpl implements IAuthEndpoint {

    @NotNull
    @Resource
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private UserService userService;

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/profile")
    public User profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String login = authentication.getName();
        return userService.findByLogin(login);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping(value = "/login", produces = "application/json")
    public Result login(@RequestParam("username") @WebParam(name = "username") final String username,
                        @RequestParam("password") @WebParam(name = "password") final String password) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping(value = "/logout", produces = "application/json")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}
