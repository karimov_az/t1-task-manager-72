package ru.karimov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.config.DatabaseConfiguration;
import ru.t1.karimov.tm.config.WebApplicationConfiguration;
import ru.t1.karimov.tm.exception.entity.TaskNotFoundException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.service.TaskService;
import ru.t1.karimov.tm.util.UserUtil;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = { WebApplicationConfiguration.class, DatabaseConfiguration.class })
public class TaskServiceTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USER_PASSWORD = "test";

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    private final Task task1 = new Task("Task Test 1");

    @NotNull
    private final Task task2 = new Task("Task Test 2");

    @NotNull
    private final Task task3 = new Task("Task Test 3");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.addByUserId(UserUtil.getUserId(), task1);
        taskService.addByUserId(UserUtil.getUserId(), task2);
        task3.setUserId(UserUtil.getUserId());
    }

    @After
    public void cleanByUserIdTest() {
        taskService.removeAllByUserId(UserUtil.getUserId());
    }

    @Test
    public void addByUserIdTest() {
        final int expectedSize = 3;
        @Nullable final String userId = UserUtil.getUserId();
        assertNotNull(userId);
        taskService.addByUserId(userId, task3);
        @NotNull final List<Task> tasks = taskService.findAllByUserId(userId);
        assertEquals(expectedSize, tasks.size());

        assertThrows(
                TaskNotFoundException.class,
                () -> taskService.addByUserId(userId, null)
        );
        assertThrows(
                UserIdEmptyException.class,
                () -> taskService.addByUserId(null, new Task("Error"))
        );
    }

    @Test
    public void findAllByUserIdTest() {
        final int expectedSize = 2;
        @Nullable final String userId = UserUtil.getUserId();
        assertNotNull(userId);
        assertEquals(expectedSize, taskService.findAllByUserId(userId).size());

        assertThrows(
                UserIdEmptyException.class,
                () -> taskService.findAllByUserId(null)
        );
    }

    @Test
    public void findByIdAndUserId() {
        @Nullable final String userId = UserUtil.getUserId();
        @NotNull final String expectedId = task1.getId();
        assertNotNull(userId);
        assertNotNull(taskService.findOneByUserIdAndId(userId, expectedId));

        assertThrows(
                IdEmptyException.class,
                () -> taskService.findOneByUserIdAndId(userId, null)
        );
        assertThrows(
                UserIdEmptyException.class,
                () -> taskService.findOneByUserIdAndId(null, "test")
        );
    }

    @Test
    public void removeByIdAndUserId() {
        @Nullable final String userId = UserUtil.getUserId();
        @NotNull final String expectedId = task1.getId();
        assertNotNull(userId);
        taskService.removeByUserIdAndId(userId, expectedId);
        assertNull(taskService.findOneByUserIdAndId(userId, expectedId));

        assertThrows(
                IdEmptyException.class,
                () -> taskService.removeByUserIdAndId(userId, null)
        );
        assertThrows(
                UserIdEmptyException.class,
                () -> taskService.removeByUserIdAndId(null, "test")
        );
    }

    @Test
    public void removeAllByUserId() {
        final int expectedSize = 0;
        @Nullable final String userId = UserUtil.getUserId();
        assertNotNull(userId);
        taskService.removeAllByUserId(userId);
        final int actualSize = taskService.findAllByUserId(userId).size();
        assertEquals(expectedSize, actualSize);

        assertThrows(
                UserIdEmptyException.class,
                () -> taskService.removeAllByUserId(null)
        );
    }

}
