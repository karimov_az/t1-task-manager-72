package ru.karimov.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.config.DatabaseConfiguration;
import ru.t1.karimov.tm.config.WebApplicationConfiguration;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.service.ProjectService;
import ru.t1.karimov.tm.util.UserUtil;

import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = { WebApplicationConfiguration.class, DatabaseConfiguration.class })
public class ProjectControllerTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USER_PASSWORD = "test";

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private final Project project1 = new Project("Test Project1");

    @NotNull
    private final Project project2 = new Project("Test Project2");

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        @Nullable final String userId = UserUtil.getUserId();

        projectService.addByUserId(userId, project1);
        projectService.addByUserId(userId, project2);
    }

    @After
    public void clean() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        final int expectedSize = 3;
        @NotNull final String url = "/project/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final List<Project> projects = projectService.findAllByUserId(UserUtil.getUserId());
        assertEquals(expectedSize, projects.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String id = project1.getId();
        @NotNull final String url = "/project/delete/" + id;

        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        assertNull(projectService.findOneByUserIdAndId(UserUtil.getUserId(), id));
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final String id = project1.getId();
        @NotNull final String url = "/project/edit/" + id;
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
