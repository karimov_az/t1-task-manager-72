package ru.karimov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.config.DatabaseConfiguration;
import ru.t1.karimov.tm.config.WebApplicationConfiguration;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.service.ProjectService;
import ru.t1.karimov.tm.util.UserUtil;

import java.util.List;

import static org.junit.Assert.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = { WebApplicationConfiguration.class, DatabaseConfiguration.class })
public class ProjectServiceTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USER_PASSWORD = "test";

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final Project project1 = new Project("Project Test 1");

    @NotNull
    private final Project project2 = new Project("Project Test 2");

    @NotNull
    private final Project project3 = new Project("Project Test 3");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        @Nullable final String userId = UserUtil.getUserId();

        projectService.addByUserId(userId, project1);
        projectService.addByUserId(userId, project2);
        project3.setUserId(userId);
    }

    @After
    public void cleanByUserIdTest() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @Test
    public void addByUserIdTest() {
        final int expectedSize = 3;
        @Nullable final String userId = UserUtil.getUserId();
        assertNotNull(userId);
        projectService.addByUserId(userId, project3);
        @NotNull final List<Project> projects = projectService.findAllByUserId(userId);
        assertEquals(expectedSize, projects.size());

        assertThrows(
                ProjectNotFoundException.class,
                () -> projectService.addByUserId(userId, null)
        );
        assertThrows(
                UserIdEmptyException.class,
                () -> projectService.addByUserId(null, new Project("Error"))
        );
    }

    @Test
    public void findAllByUserIdTest() {
        final int expectedSize = 2;
        @Nullable final String userId = UserUtil.getUserId();
        assertNotNull(userId);
        assertEquals(expectedSize, projectService.findAllByUserId(userId).size());

        assertThrows(
                UserIdEmptyException.class,
                () -> projectService.findAllByUserId(null)
        );
    }

    @Test
    public void findByIdAndUserId() {
        @Nullable final String userId = UserUtil.getUserId();
        @NotNull final String expectedId = project1.getId();
        assertNotNull(userId);
        assertNotNull(projectService.findOneByUserIdAndId(userId, expectedId));

        assertThrows(
                IdEmptyException.class,
                () -> projectService.findOneByUserIdAndId(userId, null)
        );
        assertThrows(
                UserIdEmptyException.class,
                () -> projectService.findOneByUserIdAndId(null, "test")
        );
    }

    @Test
    public void removeByIdAndUserId() {
        @Nullable final String userId = UserUtil.getUserId();
        @NotNull final String expectedId = project1.getId();
        assertNotNull(userId);
        projectService.removeByUserIdAndId(userId, expectedId);
        assertNull(projectService.findOneByUserIdAndId(userId, expectedId));

        assertThrows(
                IdEmptyException.class,
                () -> projectService.removeByUserIdAndId(userId, null)
        );
        assertThrows(
                UserIdEmptyException.class,
                () -> projectService.removeByUserIdAndId(null, "test")
        );
    }

    @Test
    public void removeAllByUserId() {
        final int expectedSize = 0;
        @Nullable final String userId = UserUtil.getUserId();
        assertNotNull(userId);
        projectService.removeAllByUserId(userId);
        final int actualSize = projectService.findAllByUserId(userId).size();
        assertEquals(expectedSize, actualSize);

        assertThrows(
                UserIdEmptyException.class,
                () -> projectService.removeAllByUserId(null)
        );
    }

}
