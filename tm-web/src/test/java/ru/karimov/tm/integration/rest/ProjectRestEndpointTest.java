package ru.karimov.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.karimov.tm.marker.IntegrationCategory;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@Category(IntegrationCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectRestEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @Nullable
    private static String sessionId;

    @NotNull
    private final Project project1 = new Project("Project Test 1");

    @NotNull
    private final Project project2 = new Project("Project Test 2");

    @NotNull
    private final Project project3 = new Project("Project Test 3");

    @NotNull
    private final Project project4 = new Project("Project Test 4");

    @NotNull
    private static ResponseEntity<Project> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Project.class);
    }

    private static ResponseEntity<List> sendRequestList(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    @BeforeClass
    public static void login() {
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, Result.class, Result.class);
        System.out.println(response);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @Nullable final String headersValue = headersResponse.getFirst(HttpHeaders.SET_COOKIE);
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(headersValue);
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Before
    public void initTest() {
        @NotNull final String url = BASE_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project1, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project2, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project3, header));
    }

    @After
    public void clean() {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Test
    public void deleteAllTest() {
        final int expectedSize = 0;
        @NotNull final String url = BASE_URL + "deleteAll/";
        @NotNull final String findUrl = BASE_URL + "findAll/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(header));
        assertEquals(expectedSize, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String id = project1.getId();
        @NotNull final String url = BASE_URL + "deleteById/" + id;
        @NotNull final ResponseEntity<Project> response = sendRequest(url, HttpMethod.POST, new HttpEntity<>(header));
        @NotNull final String urlFind = BASE_URL + "findById/" + id;
        assertNull(sendRequest(urlFind, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    public void findAllTest() {
        final int expectedSize = 3;
        @NotNull final String url = BASE_URL + "findAll/";
        assertEquals(expectedSize, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String id = project1.getId();
        @NotNull final String url = BASE_URL + "findById/" + id;
        @NotNull final ResponseEntity<Project> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
        assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Project project = response.getBody();
        assertNotNull(project);
        final String actual = project.getId();
        assertEquals(id, actual);
    }

    @Test
    public void saveTest() {
        @NotNull final String expectedName = project4.getName();
        @NotNull final String url = BASE_URL + "save/";
        @NotNull final ResponseEntity<Project> response = sendRequest(url, HttpMethod.POST, new HttpEntity<>(project4, header));
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final Project project = response.getBody();
        assertNotNull(project);
        @NotNull final String actualName = project.getName();
        assertEquals(expectedName, actualName);
    }

}
