package ru.karimov.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpHeaders;
import ru.karimov.tm.marker.IntegrationCategory;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.karimov.tm.client.IAuthSoapEndpointClient;
import ru.t1.karimov.tm.client.ITaskSoapEndpointClient;
import ru.t1.karimov.tm.model.Task;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import java.util.*;

import static org.junit.Assert.*;

@Category(IntegrationCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TaskSoapEndpointTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USER_PASSWORD = "test";

    @NotNull
    private static final String BASE_URL = "http://localhost:8080";

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static ITaskEndpoint taskEndpoint;

    @NotNull
    private final Task task1 = new Task("Task Test 1");

    @NotNull
    private final Task task2 = new Task("Task Test 2");

    @NotNull
    private final Task task3 = new Task("Task Test 3");

    @NotNull
    private final Task task4 = new Task("Task Test 4");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = IAuthSoapEndpointClient.getInstance(BASE_URL);
        assertTrue(authEndpoint.login(USERNAME, USER_PASSWORD).getSuccess());
        taskEndpoint = ITaskSoapEndpointClient.getInstance(BASE_URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void initTest() {
        taskEndpoint.save(task1);
        taskEndpoint.save(task2);
        taskEndpoint.save(task3);
    }

    @After
    @SneakyThrows
    public void clean() {
        taskEndpoint.deleteAll();
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String id = task1.getId();
        taskEndpoint.delete(id);
        assertNull(taskEndpoint.findById(id));
    }

    @Test
    public void findAllTest() {
        final int expectedSize = 3;
        @Nullable final Collection<Task> tasks = taskEndpoint.findAll();
        assertEquals(expectedSize, tasks.size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String expectedId = task1.getId();
        @Nullable final Task task = taskEndpoint.findById(expectedId);
        assertNotNull(task);
        final String actualId = task.getId();
        assertEquals(expectedId, actualId);
    }

    @Test
    public void saveTest() {
        @NotNull final String expectedName = task4.getName();
        @Nullable final Task task = taskEndpoint.save(task4);
        assertNotNull(task);
        @NotNull final String actualName = task.getName();
        assertEquals(expectedName, actualName);
    }

}
