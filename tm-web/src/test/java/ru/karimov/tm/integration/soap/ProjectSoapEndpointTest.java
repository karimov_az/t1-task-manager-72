package ru.karimov.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpHeaders;
import ru.karimov.tm.marker.IntegrationCategory;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.karimov.tm.client.IAuthSoapEndpointClient;
import ru.t1.karimov.tm.client.IProjectSoapEndpointClient;
import ru.t1.karimov.tm.model.Project;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

import static org.junit.Assert.*;

@Category(IntegrationCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectSoapEndpointTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USER_PASSWORD = "test";

    @NotNull
    private static final String BASE_URL = "http://localhost:8080";

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static IProjectEndpoint projectEndpoint;

    @NotNull
    private final Project project1 = new Project("Project Test 1");

    @NotNull
    private final Project project2 = new Project("Project Test 2");

    @NotNull
    private final Project project3 = new Project("Project Test 3");

    @NotNull
    private final Project project4 = new Project("Project Test 4");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = IAuthSoapEndpointClient.getInstance(BASE_URL);
        assertTrue(authEndpoint.login(USERNAME, USER_PASSWORD).getSuccess());
        projectEndpoint = IProjectSoapEndpointClient.getInstance(BASE_URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void initTest() {
        projectEndpoint.save(project1);
        projectEndpoint.save(project2);
        projectEndpoint.save(project3);
    }

    @After
    @SneakyThrows
    public void clean() {
        projectEndpoint.deleteAll();
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String id = project1.getId();
        projectEndpoint.delete(id);
        assertNull(projectEndpoint.findById(id));
    }

    @Test
    public void findAllTest() {
        final int expectedSize = 3;
        @NotNull final Collection<Project> projects = projectEndpoint.findAll();
        Assert.assertEquals(expectedSize, projects.size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String expectedId = project1.getId();
        @Nullable final Project project = projectEndpoint.findById(expectedId);
        assertNotNull(project);
        final String actualId = project.getId();
        assertEquals(expectedId, actualId);
    }

    @Test
    public void saveTest() {
        @NotNull final String expectedName = project4.getName();
        @Nullable final Project project = projectEndpoint.save(project4);
        assertNotNull(project);
        @NotNull final String actualName = project.getName();
        assertEquals(expectedName, actualName);
    }

}
