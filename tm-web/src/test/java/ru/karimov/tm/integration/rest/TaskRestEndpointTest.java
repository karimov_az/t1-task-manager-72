package ru.karimov.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.karimov.tm.marker.IntegrationCategory;
import ru.t1.karimov.tm.model.Result;
import ru.t1.karimov.tm.model.Task;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@Category(IntegrationCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TaskRestEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @Nullable
    private static String sessionId;

    @NotNull
    private final Task task1 = new Task("Task Test 1");

    @NotNull
    private final Task task2 = new Task("Task Test 2");

    @NotNull
    private final Task task3 = new Task("Task Test 3");

    @NotNull
    private final Task task4 = new Task("Task Test 4");

    private static ResponseEntity<Task> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Task.class);
    }

    private static ResponseEntity<List> sendRequestList(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    @BeforeClass
    public static void login() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, Result.class, Result.class);
        System.out.println(response);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @Nullable final String headersValue = headersResponse.getFirst(HttpHeaders.SET_COOKIE);
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(headersValue);
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Before
    public void initTest() {
        @NotNull final String url = BASE_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task1, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task2, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task3, header));
    }

    @After
    public void clean() {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Test
    public void deleteAllTest() {
        final int expectedSize = 0;
        @NotNull final String url = BASE_URL + "deleteAll/";
        @NotNull final String findUrl = BASE_URL + "findAll/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(header));
        Assert.assertEquals(expectedSize, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String id = task1.getId();
        @NotNull final String url = BASE_URL + "deleteById/" + id;
        @NotNull final ResponseEntity<Task> response = sendRequest(url, HttpMethod.POST, new HttpEntity<>(header));
        @NotNull final String urlFind = BASE_URL + "findById/" + id;
        assertNull(sendRequest(urlFind, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    public void findAllTest() {
        final int expectedSize = 3;
        @NotNull final String url = BASE_URL + "findAll/";
        Assert.assertEquals(expectedSize, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String id = task1.getId();
        @NotNull final String url = BASE_URL + "findById/" + id;
        @NotNull final ResponseEntity<Task> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final Task project = response.getBody();
        assertNotNull(project);
        final String actual = project.getId();
        assertEquals(id, actual);
    }

    @Test
    public void saveTest() {
        @NotNull final String expected = task4.getName();
        @NotNull final String url = BASE_URL + "save/";
        @NotNull final ResponseEntity<Task> response = sendRequest(url, HttpMethod.POST, new HttpEntity<>(task4, header));
        assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Task task = response.getBody();
        assertNotNull(task);
        @NotNull final String actual = task.getName();
        assertEquals(expected, actual);
    }

}
